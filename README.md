# ioc-bot

This GitLab bot responds to webhooks from repositories part of the [ioc] group.

It is used to:

- update (on push events) the [iocs] repository that contains all IOC repositories as submodules.
- delete submodules (on project destroy, archive or transfer events) from the [iocs] repository.
- sync the projects from the [ioc] group and [iocs] repository submodules (manual command to run periodically).


Configuration
-------------

1. Create two webhooks in GitLab. Both should use the same secret token.

   - a webhook on the [ioc] group for **Push** events.
   - a system hook to be triggered on **Repository update events** (this is to delete submodules when a project
     of the ioc group is destroyed).

2. Add a SSH key to a bot user in GitLab to clone and push to the [iocs] repository (including all the submodules). Add the user
   to the [iocs] repo and [ioc] group as maintainer.

3. Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) to allow the bot user
   to access GitLab API.

4. The following environment variables shall be passed to the bot:

   - GL_SECRET: the token secret used when creating the webhooks.
   - IOC_BOT_SSH_KEY: the private key corresponding to the public key added to the bot user in GitLab.
   - GL_ACCESS_TOKEN: the personal access token to allow operations via the API.

   This is done by the [ics-ans-role-ioc-bot](https://gitlab.esss.lu.se/ics-ansible-galaxy/ics-ans-role-ioc-bot) Ansible role.

License
-------

MIT

[iocs]: https://gitlab.esss.lu.se/ics-infrastructure/iocs
[ioc]: https://gitlab.esss.lu.se/ioc
