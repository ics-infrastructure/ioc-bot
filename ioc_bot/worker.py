import logging
import subprocess
import sentry_sdk
from pathlib import Path
from redis import Redis
from rq import Connection, Worker
from sentry_sdk.integrations.rq import RqIntegration
from .settings import (
    GITLAB_MAIN_PROJECT,
    REDIS_URL,
    QUEUES,
    GIT_WORKING_DIR,
    IOC_BOT_SSH_KEY,
    SENTRY_DSN,
)
from .util import run

logger = logging.getLogger(__name__)


def setup_git():
    """Setup git config and the private ssh key"""
    ssh_private_key = Path.home() / ".ssh" / "id_rsa"
    if not ssh_private_key.exists():
        logger.info("Setup ssh key and git config")
        with ssh_private_key.open("w") as f:
            f.write(IOC_BOT_SSH_KEY + "\n")
        ssh_private_key.chmod(0o600)
        run("git config --global user.name ioc-bot")
        run("git config --global user.email ioc-bot@esss.se")


def setup_main_repo():
    """Clone the main repository"""
    p = Path(GIT_WORKING_DIR) / GITLAB_MAIN_PROJECT
    if not p.exists():
        # Make sure GIT_WORKING_DIR exists
        p.parent.mkdir(mode=0o755, exist_ok=True)
        logger.info(f"Clone {GITLAB_MAIN_PROJECT} repo")
        run(
            f"git clone --recurse-submodules git@gitlab.esss.lu.se:ics-infrastructure/{GITLAB_MAIN_PROJECT}.git",
            cwd=GIT_WORKING_DIR,
        )
    else:
        logger.info(f"Make sure {GITLAB_MAIN_PROJECT} repo is up-to-date")
        run("git pull --rebase", cwd=p)


def run_worker():
    """Start a worker"""
    if SENTRY_DSN:
        sentry_sdk.init(SENTRY_DSN, integrations=[RqIntegration()])
    try:
        setup_git()
        setup_main_repo()
    except subprocess.CalledProcessError:
        if SENTRY_DSN:
            sentry_sdk.capture_exception()
        raise
    with Connection(Redis.from_url(REDIS_URL)):
        w = Worker(QUEUES)
        w.work()
