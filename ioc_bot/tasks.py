import os
import logging
from pathlib import Path
from rq import Queue
from redis import Redis
from . import util
from .util import run
from .settings import REDIS_URL, GIT_WORKING_DIR, GITLAB_MAIN_PROJECT

logger = logging.getLogger(__name__)

redis_conn = Redis.from_url(REDIS_URL)
q = Queue(connection=redis_conn)


def prepare_repo():
    os.chdir(Path(GIT_WORKING_DIR) / GITLAB_MAIN_PROJECT)
    if not util.is_main_repo():
        raise FileNotFoundError(f"Couldn't find the {GITLAB_MAIN_PROJECT} repo")
    logger.info(f"Make sure {GITLAB_MAIN_PROJECT} repo is up-to-date")
    run("git pull --rebase")


def update_submodule(module_name, path_with_namespace, local_path=".", push=True):
    """Update the given submodule in the main repository"""
    if push:
        prepare_repo()
    p = Path(local_path) / module_name
    if p.exists():
        logger.info(f"Updating submodule {p}")
        run(f"git submodule update --remote {p}")
        # Allow the command to fail if there is nothing to commit
        # (we were already up to date)
        run(f"git commit -m 'Update {p}'", check=False)
    else:
        logger.info(f"Adding new submodule {p}")
        run(f"git submodule add --force ../../{path_with_namespace}.git {p}")
        run(f"git commit -m 'Add {p}'")
    if push:
        run("git push origin master")


def delete_submodule(module_name, local_path=".", push=True):
    """Delete the given submodule in the main repository"""
    if push:
        prepare_repo()
    p = Path(local_path) / module_name
    if p.exists():
        logger.info(f"Removing {p}")
        run(f"git rm -rf {p}")
        run(f"git commit -m 'Remove {p}'")
        if push:
            run("git push origin master")
    else:
        logger.info(f"{p} doesn't exist")


def synchronize_repo(group_projects):
    prepare_repo()
    repo_submodules = set(util.list_submodules())
    remote_repos = set(group_projects.keys())
    logger.debug(f"Repo submodules {repo_submodules}")
    logger.debug(f"Remote repos {remote_repos}")
    repos_to_add = remote_repos - repo_submodules
    for name in repos_to_add:
        update_submodule(name, group_projects[name], push=False)
    repos_to_delete = repo_submodules - remote_repos
    for name in repos_to_delete:
        delete_submodule(name, push=False)
    logger.info("Make sure all submodules are up-to-date")
    run(f"git submodule update --remote")
    # Allow the command to fail if there is nothing to commit
    # (we were already up to date)
    run(f"git commit -m 'Update all submodules'", check=False)
    run("git push origin master")
