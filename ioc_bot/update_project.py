import gidgetlab.routing
from . import tasks
from .settings import GITLAB_SM_GROUP

router = gidgetlab.routing.Router()


@router.register("Push Hook")
async def update_repo(event, gl, *args, **kwargs):
    """Update the main repo with the submodule that was pushed"""
    branch = event.data["ref"].replace("refs/heads/", "")
    if branch != "master":
        # Only look at the master branch
        return None
    tasks.q.enqueue(
        tasks.update_submodule,
        event.data["project"]["name"],
        event.data["project"]["path_with_namespace"],
    )


@router.register("System Hook")
async def delete_submodule(event, gl, *args, **kwargs):
    """Delete the submodule when the repo was destroyed, archived or transfered"""
    if event.data["event_name"] == "project_transfer":
        # Remove the old name (the new name will be added when pushing to it)
        # Note that if subgroups are used, old_path can include "/"
        old_namespace, old_path = event.data["old_path_with_namespace"].split("/", 1)
        if old_namespace == GITLAB_SM_GROUP:
            tasks.q.enqueue(tasks.delete_submodule, event.data["name"])
        return
    if not event.data.get("path_with_namespace", "").startswith(f"{GITLAB_SM_GROUP}/"):
        return
    if event.data["event_name"] == "project_destroy":
        tasks.q.enqueue(tasks.delete_submodule, event.data["name"])
    elif event.data["event_name"] == "project_update":
        # check if the project was archived
        data = await gl.getitem(f"/projects/{event.data['project_id']}")
        if data["archived"]:
            tasks.q.enqueue(tasks.delete_submodule, data["name"])
