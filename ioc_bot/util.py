import aiohttp
import logging
import shlex
import subprocess
import gidgetlab.aiohttp as gl_aiohttp
from pathlib import Path
from .settings import GL_ACCESS_TOKEN, GITLAB_MAIN_PROJECT


logger = logging.getLogger(__name__)


def run(cmd, cwd=None, check=True, no_log=False):
    """Run the shell command and return the result"""
    if not no_log:
        logger.info(f"Run '{cmd}'")
    args = shlex.split(cmd)
    result = subprocess.run(args, check=check, capture_output=True, cwd=cwd)
    output = result.stdout.decode("utf-8").strip()
    if output and not no_log:
        logger.info(output)
    return output


def is_main_repo():
    """Return True if the current directory is the main repo"""
    result = run("git remote -v")
    return f"ics-infrastructure/{GITLAB_MAIN_PROJECT}.git" in result


def list_submodules(cwd=None):
    output = run("git submodule--helper list", cwd=cwd)
    if not output:
        return []
    return [Path(line.split()[-1]).name for line in output.split("\n")]


async def list_group_projects(group_id):
    """Return a list of projects in this group"""
    async with aiohttp.ClientSession() as session:
        gl = gl_aiohttp.GitLabAPI(
            session,
            "ioc-bot",
            url="https://gitlab.esss.lu.se",
            access_token=GL_ACCESS_TOKEN,
        )
        # By default archived projects are not returned, but it's safer to be explicit
        async for project in gl.getiter(
            f"/groups/{group_id}/projects", params={"archived": False}
        ):
            yield project
