import os

REDIS_URL = os.environ.get("REDIS_URL", "redis://redis:6379/0")

# Queues to listen on
QUEUES = ["default"]

GIT_WORKING_DIR = os.environ.get("GIT_WORKING_DIR", "/home/csi/data")
# Name of the group with all IOC repositories
GITLAB_SM_GROUP = "ioc"
# Name of the project with all IOC repositories as submodules
GITLAB_MAIN_PROJECT = "iocs"
# The private key used to clone git repositories
IOC_BOT_SSH_KEY = os.environ.get("IOC_BOT_SSH_KEY", "private-key")
# The personal access token to use the GitLab API
GL_ACCESS_TOKEN = os.environ.get("GL_ACCESS_TOKEN", "secret-token")

# Sentry Data Source Name
# Leave it empty to disable it
SENTRY_DSN = os.environ.get("SENTRY_DSN", "")
