import asyncio
import logging
import click
import sentry_sdk
import os
from sentry_sdk.integrations.aiohttp import AioHttpIntegration
from gidgetlab.aiohttp import GitLabBot
from .settings import GITLAB_SM_GROUP
from . import update_project, tasks, util
from . import worker as worker_module


sentry_dsn = os.environ.get("IOC_BOT_SENTRY_DSN")
if sentry_dsn:
    sentry_sdk.init(dsn=sentry_dsn, integrations=[AioHttpIntegration()])
bot = GitLabBot("ioc-bot", url="https://gitlab.esss.lu.se")
bot.register_routers(update_project.router)


async def sync_repo():
    group_projects = {
        project["name"]: project["path_with_namespace"] async
        for project in util.list_group_projects(GITLAB_SM_GROUP)
        if project["default_branch"] is not None
    }
    tasks.q.enqueue(tasks.synchronize_repo, group_projects)


@click.group()
@click.version_option()
def cli():
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )


@cli.command()
def run():
    bot.run()


@cli.command()
def sync():
    asyncio.run(sync_repo())


@cli.command()
def worker():
    worker_module.run_worker()


if __name__ == "__main__":
    cli()
