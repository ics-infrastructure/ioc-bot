import pytest
from gidgetlab import sansio
from ioc_bot import update_project, tasks
from .utils import FakeGitLab


@pytest.mark.asyncio
async def test_update_repo_master_branch(mocker):
    gl = FakeGitLab()
    mock_enqueue = mocker.patch("ioc_bot.update_project.tasks.q.enqueue")
    sha = "8b9f3881cae5387dc969e66a03515cfe099cf088"
    name = "a.project"
    path_with_namespace = "test-group/a_project"
    data = {
        "ref": "refs/heads/master",
        "checkout_sha": sha,
        "project": {
            "id": 551,
            "name": name,
            "path_with_namespace": path_with_namespace,
        },
    }
    event = sansio.Event(data, event="Push Hook")
    await update_project.router.dispatch(event, gl)
    mock_enqueue.assert_called_once_with(
        tasks.update_submodule, name, path_with_namespace
    )


@pytest.mark.asyncio
async def test_update_repo_not_master_branch(mocker):
    gl = FakeGitLab()
    mock_enqueue = mocker.patch("ioc_bot.update_project.tasks.q.enqueue")
    sha = "8b9f3881cae5387dc969e66a03515cfe099cf088"
    name = "a.project"
    path_with_namespace = "test-group/a_project"
    data = {
        "ref": "refs/heads/test-branch",
        "checkout_sha": sha,
        "project": {
            "id": 551,
            "name": name,
            "path_with_namespace": path_with_namespace,
        },
    }
    event = sansio.Event(data, event="Push Hook")
    await update_project.router.dispatch(event, gl)
    assert not mock_enqueue.called


@pytest.mark.asyncio
async def test_delete_submodule_on_project_destroy(mocker):
    """Check that the task is called when a project from GITLAB_SM_GROUP is destroyed"""
    mock_enqueue = mocker.patch("ioc_bot.update_project.tasks.q.enqueue")
    data = {
        "event_name": "project_destroy",
        "name": "my-project",
        "path": "my-project",
        "path_with_namespace": "ioc/my-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_project.router.dispatch(event, gl)
    mock_enqueue.assert_called_once_with(tasks.delete_submodule, "my-project")


@pytest.mark.asyncio
async def test_delete_submodule_on_project_transfer(mocker):
    """Check that the task is called when a project from GITLAB_SM_GROUP is transfered"""
    mock_enqueue = mocker.patch("ioc_bot.update_project.tasks.q.enqueue")
    data = {
        "event_name": "project_transfer",
        "name": "my-project",
        "path": "my-project",
        "path_with_namespace": "new-group/my-project",
        "old_path_with_namespace": "ioc/my-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_project.router.dispatch(event, gl)
    mock_enqueue.assert_called_once_with(tasks.delete_submodule, "my-project")


@pytest.mark.asyncio
async def test_delete_submodule_on_project_archive(mocker):
    """Check that the task is called when a project from GITLAB_SM_GROUP is archived"""
    mock_enqueue = mocker.patch("ioc_bot.update_project.tasks.q.enqueue")
    project_id = 42
    data = {
        "event_name": "project_update",
        "project_id": project_id,
        "path_with_namespace": "ioc/deprecated-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = FakeGitLab(getitem={"name": "deprecated-project", "archived": True})
    await update_project.router.dispatch(event, gl)
    assert gl.getitem_url == f"/projects/{project_id}"
    mock_enqueue.assert_called_once_with(tasks.delete_submodule, "deprecated-project")


@pytest.mark.asyncio
async def test_delete_submodule_not_called_on_project_update(mocker):
    """Check that the task is not called when a project from GITLAB_SM_GROUP is just updated (not archived)"""
    mock_enqueue = mocker.patch("ioc_bot.update_project.tasks.q.enqueue")
    project_id = 43
    data = {
        "event_name": "project_update",
        "project_id": project_id,
        "path_with_namespace": "ioc/my-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = FakeGitLab(getitem={"path": "my-project", "archived": False})
    await update_project.router.dispatch(event, gl)
    assert gl.getitem_url == f"/projects/{project_id}"
    assert not mock_enqueue.called


@pytest.mark.asyncio
@pytest.mark.parametrize(
    "event_name",
    (
        "project_create",
        "user_add_to_team",
        "user_remove_from_team",
        "user_create",
        "user_destroy",
        "user_failed_login",
        "user_rename",
        "key_create",
        "key_destroy",
        "group_create",
        "group_destroy",
        "foo",
    ),
)
async def test_delete_submodule_not_called_on_other_events(mocker, event_name):
    """Check that the task is not called on other events"""
    mock_enqueue = mocker.patch("ioc_bot.update_project.tasks.q.enqueue")
    data = {"event_name": event_name}
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_project.router.dispatch(event, gl)
    assert not mock_enqueue.called


@pytest.mark.asyncio
@pytest.mark.parametrize("event_name", ("project_destroy", "project_update"))
async def test_delete_submodule_not_called_on_other_group(mocker, event_name):
    """Check that the task is not called on project destroy or update from another group"""
    mock_enqueue = mocker.patch("ioc_bot.update_project.tasks.q.enqueue")
    data = {
        "event_name": event_name,
        "name": "my-project",
        "path": "my-project",
        "path_with_namespace": "my-group/my-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_project.router.dispatch(event, gl)
    assert not mock_enqueue.called


@pytest.mark.asyncio
@pytest.mark.parametrize("event_name", ("project_transfer", "project_rename"))
async def test_delete_submodule_not_called_on_different_old_group(mocker, event_name):
    """Check that the task is not called on project transfer or rename from another group"""
    mock_enqueue = mocker.patch("ioc_bot.update_project.tasks.q.enqueue")
    data = {
        "event_name": event_name,
        "name": "my-project",
        "path": "my-project",
        "path_with_namespace": "ioc/my-project",
        "old_path_with_namespace": "my-group/my-cool-project",
    }
    event = sansio.Event(data, event="System Hook")
    gl = None
    await update_project.router.dispatch(event, gl)
    assert not mock_enqueue.called
