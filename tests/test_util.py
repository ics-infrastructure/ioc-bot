import logging
import subprocess
import pytest
from ioc_bot import util


def test_run_success_command_with_output(tmpdir, caplog):
    caplog.set_level(logging.INFO)
    p = tmpdir.join("foo")
    p.write("Hello!")
    result = util.run("cat foo", cwd=tmpdir)
    assert len(caplog.record_tuples) == 2
    assert "Run 'cat foo'" in caplog.text
    assert "Hello!" in caplog.text
    assert result == "Hello!"


def test_run_success_command_with_no_output(tmpdir, caplog):
    caplog.set_level(logging.INFO)
    util.run("mkdir test", cwd=tmpdir)
    assert len(caplog.record_tuples) == 1
    assert "Run 'mkdir test'" in caplog.text


def test_run_fail_check_true(tmpdir):
    with pytest.raises(subprocess.CalledProcessError):
        util.run("ls no-file", cwd=tmpdir)


def test_run_fail_check_false(tmpdir):
    result = util.run("ls no-file", cwd=tmpdir, check=False)
    assert result == ""


def test_run_no_log(caplog):
    caplog.set_level(logging.INFO)
    util.run("echo secret", no_log=True)
    assert "secret" not in caplog.text
    assert len(caplog.record_tuples) == 0
